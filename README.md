## Draw Svg pai

## Description
3 api included
- dummy 'hello world' api
- get api for svg rectangle dimension
- post api for svg rectangle dimension

## Installation
- dotnet-sdk-6.0.202 must be installed to run this project.

## Run Project
- dotnet run

## Instructions
- first run api server with  'dotnet run'
- after that run the front end project with 'ng serve'
- run 'npm install' before 'ng serve'