using System.Text.Json;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddCors(p => p.AddPolicy("corsApp", corsBuilder =>
{
    corsBuilder.WithOrigins("*").AllowAnyMethod().AllowAnyHeader();
}));

var app = builder.Build();

app.UseCors("corsApp");

app.MapGet("/figure", async () =>
{
    await using FileStream stream = File.OpenRead(Directory.GetCurrentDirectory() + @"\svg-draw-api-data.json");
    return await JsonSerializer.DeserializeAsync<Square>(stream);
});

app.MapPost("/figure", async (Square square) =>
{
    try
    {
        await using FileStream createStream = File.Create(Directory.GetCurrentDirectory() + @"\svg-draw-api-data.json");
        await JsonSerializer.SerializeAsync(createStream, square);
        return "updated successfully";
    }
    catch
    {
        return "something went wrong";
    }
    
});

app.MapGet("/", () => "Hello World!");




app.Run();

class Square
{
    public int Height { get; set; }
    public int Width { get; set; }
}